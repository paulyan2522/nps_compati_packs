require 'csv'
require 'open-uri'

begin
  
  games = CSV.parse(open("http://beta.nopaystation.com/tsv/PSV_GAMES.tsv"), { :col_sep => "\t", :headers => true, encoding: "ISO8859-1"})

filtered = games

result = ""
Dir["patch/PCS*/*.ppk"].each do |game|
	game_title_id = game.split("/")[1]
	game_name = filtered.find {|i| i["Title ID"] == game.split("/")[1] }["Name"]
	result += "#{game}=#{game_name}\n"
end

File.write("entries_patch.txt", result)

rescue
  
end
